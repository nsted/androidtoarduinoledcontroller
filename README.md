Read Serial input with an Arduino and use it to control patterns on LED strips.
Accompanies, androidAppSerialOutput repository, which is a Cordova Android app 
that outputs the Serial data.